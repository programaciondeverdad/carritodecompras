DROP TABLE `metodos_entrega`;
CREATE TABLE `metodos_entrega` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(250) NULL,
  `estado` VARCHAR(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`));



DROP TABLE `metodos_entrega_rangos`;
CREATE TABLE `metodos_entrega_rangos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `metodo_entrega_id` INT(11) NOT NULL,
  `hora_inicio` TIME NOT NULL,
  `hora_fin` TIME NOT NULL,
  `descripcion` VARCHAR(250) NULL,
  `estado` VARCHAR(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  INDEX `fk_metodos_entrega_metodos_entrega_rangos_idx` (`metodo_entrega_id` ASC),
  CONSTRAINT `fk_metodos_entrega_metodos_entrega_rangos`
    FOREIGN KEY (`metodo_entrega_id`)
    REFERENCES `carrito_compras`.`metodos_entrega` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `carritos` 
ADD COLUMN `metodo_entrega_rango_id` INT(11) NULL AFTER `metodo_entrega_id`,
ADD INDEX `cerrito_entrega_rango_idx` (`metodo_entrega_rango_id` ASC);
;
ALTER TABLE `carritos` 
ADD CONSTRAINT `carrito_entrega_rango`
  FOREIGN KEY (`metodo_entrega_rango_id`)
  REFERENCES `metodos_entrega_rangos` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


INSERT INTO `metodos_entrega` (`nombre`, `descripcion`, `estado`) VALUES ('Retiro en sucursal', 'Retirar el pedido en nuestra sucursal ubicada en XXXX.', 'A');
INSERT INTO `metodos_entrega` (`nombre`, `descripcion`, `estado`) VALUES ('Entrega a domicilio', 'Recibir el pedido en tu domicilio', 'A');

INSERT INTO `metodos_entrega_rangos` (`metodo_entrega_id`, `hora_inicio`, `hora_fin`, `descripcion`, `estado`) VALUES ('1', '10:00', '16:00', 'Turno mañana', 'A');
INSERT INTO `metodos_entrega_rangos` (`metodo_entrega_id`, `hora_inicio`, `hora_fin`, `descripcion`, `estado`) VALUES ('1', '18:00', '20:00', 'Turno noche', 'A');
INSERT INTO `metodos_entrega_rangos` (`metodo_entrega_id`, `hora_inicio`, `hora_fin`, `descripcion`, `estado`) VALUES ('2', '10:00', '13:00', 'Turno mañana', 'A');
INSERT INTO `metodos_entrega_rangos` (`metodo_entrega_id`, `hora_inicio`, `hora_fin`, `descripcion`, `estado`) VALUES ('2', '13:00', '17:30', 'Turno tarde', 'A');
INSERT INTO `metodos_entrega_rangos` (`metodo_entrega_id`, `hora_inicio`, `hora_fin`, `descripcion`, `estado`) VALUES ('2', '17:30', '21:30', 'Turno noche', 'A');

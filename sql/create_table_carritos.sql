CREATE TABLE `carrito_compras`.`carritos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `usuario_id` INT NOT NULL,
  `metodo_pago_id` INT NULL,
  `fecha_pago` DATETIME NULL,
  `metodo_entrega_id` INT NULL,
  `fecha_entrega` DATETIME NULL,
  `fecha_registro` DATETIME NOT NULL,
  `fecha_modificacion` DATETIME NULL,
  `estado` VARCHAR(1) NOT NULL DEFAULT 'P' COMMENT 'Pendiente: P\nCancelado: C\nFinalizado: F\nAprobado: A',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `usuario_id_UNIQUE` (`usuario_id` ASC),
  UNIQUE INDEX `fecha_pago_UNIQUE` (`fecha_pago` ASC),
  UNIQUE INDEX `fecha_entrega_UNIQUE` (`fecha_entrega` ASC),
  CONSTRAINT `carrito_x_usuario`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `carrito_compras`.`usuarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- Proximamente agregaremos FKs de metodos de pago y metodos de envio
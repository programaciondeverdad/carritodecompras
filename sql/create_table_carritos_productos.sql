CREATE TABLE `carrito_compras`.`carritos_productos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `carrito_id` INT NOT NULL,
  `producto_id` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `precio` DECIMAL(6,2) NOT NULL COMMENT 'El valor puede cambiar en un futuro. Guardamos el precio aqui!',
  `comentarios` VARCHAR(80) NULL,
  `hay_stock` VARCHAR(1) NULL DEFAULT 'S' COMMENT 'Si había o no stock a la hora de armar el pedido. Para dejar un seguimiento de lo que sucedió.\nSí: S\nNo: N',
  `estado` VARCHAR(1) NULL DEFAULT 'P' COMMENT 'Al agregar el producto a la hora de hacer el envio/entrega del pedido.\nAprobado: A\nCancelado: C\nPendiente: P',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `carrito_id_UNIQUE` (`carrito_id` ASC),
  UNIQUE INDEX `producto_id_UNIQUE` (`producto_id` ASC),
  CONSTRAINT `carritos_productos_carrito_id`
    FOREIGN KEY (`carrito_id`)
    REFERENCES `carrito_compras`.`carritos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `carritos_productos_producto_id`
    FOREIGN KEY (`producto_id`)
    REFERENCES `carrito_compras`.`productos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Esta tabla tendrá la vinculación entre la compra (carrito) y cada producto que tenga adentro.';

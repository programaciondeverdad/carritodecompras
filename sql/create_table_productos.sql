DROP TABLE `productos`;
CREATE TABLE `productos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(255) NULL,
  `precio` DECIMAL(6,2) NULL,
  `categoria_id` INT NULL,
  `fecha_alta` DATE NOT NULL,
  `fecha_modificacion` DATE NULL,
  `estado` VARCHAR(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`));

INSERT INTO `productos` (`nombre`, `precio`, `fecha_alta`, `estado`) VALUES ('Manzanas 1kg', '10', '2020-06-13', 'A');
INSERT INTO `productos` (`nombre`, `precio`, `fecha_alta`, `estado`) VALUES ('Bananas 500g', '30.5', '2020-06-13', 'A');
INSERT INTO `productos` (`nombre`, `precio`, `fecha_alta`, `estado`) VALUES ('Papas 1kg', '31.8', '2020-06-13', 'A');
INSERT INTO `productos` (`nombre`, `precio`, `fecha_alta`, `estado`) VALUES ('Acelga 1 unidad', '6.5', '2020-06-13', 'A');
INSERT INTO `productos` (`nombre`, `precio`, `fecha_alta`, `estado`) VALUES ('Espinaca 1 unidad', '8.5', '2020-06-13', 'A');

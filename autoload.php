<?php
$path_root = "C:\\xampp\htdocs\pdv\carritocompras";
function listarArchivos( $path ){
    // Abrimos la carpeta que nos pasan como parámetro
    $path = $path . "/";
    $dir = opendir($path);
    // Leo todos los ficheros de la carpeta
    while ($elemento = readdir($dir)){
        // Tratamos los elementos . y .. que tienen todas las carpetas
        if( $elemento != "." && $elemento != ".."){
            // Si es una carpeta
            if( is_dir($path.$elemento) ){
                // Muestro la carpeta
				listarArchivos($path.$elemento);
            // Si es un fichero
            } else {
                if(obtenerExtensionFichero($elemento) == "php")
                {
                    // Incluyo el fichero php
    				require_once $path.$elemento;
                }
            }
        }
    }
}
function obtenerExtensionFichero($str)
{
    $explode = explode(".", $str);
    return end($explode);
}
// Llamamos a la función para que nos muestre el contenido de la carpeta gallery
listarArchivos($path_root."/config");
listarArchivos($path_root."/src");
?>
<?php
include __DIR__.'/../autoload.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Carrito Compras</title>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
<body>
	<h1>Carrito de compras - Inicio</h1>
	Menu navegación:<br>
	<ul>
		<li><a href="login.php">Iniciar Sesión</a></li>
		<li><a href="registro.php">Registrarse</a></li>
		<li><a href="contacto.php">Contacto</a></li>
	</ul>


	<div>
		<h1>Productos</h1>
		<div id='listado_productos'>
			<!-- Productos dinámicos -->
		</div>
	</div>

	<script type="text/javascript" src="assets/js/ajax.class.js"></script>
	<script type="text/javascript" src="assets/js/globals.js"></script>
	<script type="text/javascript" src="assets/js/index.js"></script>
</body>
</html>
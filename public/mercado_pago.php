<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
var_dump($_POST);

if(!isset($_POST['btnSubmit']))
{
	header('Location: metodo_pago.php');
}
include __DIR__.'/../autoload.php';

// SDK de Mercado Pago
require __DIR__ .  '/../vendor/autoload.php';

// Agrega credenciales
MercadoPago\SDK::setAccessToken('TEST_ACCESS_TOKEN');

// Crea un objeto de preferencia
$preference = new MercadoPago\Preference();

$items = array();



if(isset($_POST['items']) && count($_POST['items']))
{
	foreach ($_POST['items'] as $valor) {
		if($valor > 0)
		{
			$item = new MercadoPago\Item();
			//$item->title = strtoupper($nombre) . " x". $cantidad;
			$item->quantity = 1;
			$item->currency_id = "ARS";
			$item->unit_price = floatval($valor);
			array_push($items, $item);
		}
	}
}

$preference->items = $items;
// var_dump($items);
$preference->save();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>Pagar con: Mercado pago</title>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
<body>
	<h1>Pagar</h1>
	Menu navegación:<br>
	<ul>
		<li><a href="login.php">Iniciar Sesión</a></li>
		<li><a href="registro.php">Registrarse</a></li>
		<li><a href="mi_carrito.php">Mi Carrito <?php echo "ID carrito: ".$_SESSION['idCarrito']; ?></a></li>
	</ul>
	<form action="/procesar-pago" method="POST">
	  <script
	   src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
	   data-preference-id="<?php echo $preference->id; ?>">
	  </script>
	</form>
</body>
</html>
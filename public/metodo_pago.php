<?php
include __DIR__.'/../autoload.php';
session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Carrito Compras</title>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
<body>
	<h1>Pagar</h1>
	Menu navegación:<br>
	<ul>
		<li><a href="login.php">Iniciar Sesión</a></li>
		<li><a href="registro.php">Registrarse</a></li>
		<li><a href="mi_carrito.php">Mi Carrito <?php echo "ID carrito: ".$_SESSION['idCarrito']; ?></a></li>
	</ul>

	<h2>Pagar con mercado libre</h2>
	<form method="POST" action='mercado_pago.php'>
		<div id='listado'>
		</div>
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Pagar">
	</form>
	<br>

	<script type="text/javascript" src="assets/js/globals.js"></script>
	<script type="text/javascript" src="assets/js/ajax.class.js"></script>
	<script type="text/javascript" src="assets/js/carrito.js"></script>
	<script type="text/javascript" src="assets/js/metodo_pago.js"></script>
	<script type="text/javascript">
		window.addEventListener("load", function() {
			var carrito = new Carrito();
			var listado = window.document.getElementById('listado');
			carrito.verInputHiddens(listado);
		});
	</script>
</body>
</html>
<?php
include __DIR__.'/../autoload.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Carrito de Compras - Iniciar Sesión</title>
</head>
<body>
	<h1>Carrito de Compras</h1>
	<h2>Iniciar Sesión</h2>
	<!-- Si el incio de sesión de todo OK, entonces continua al index.php -->
	<form id="form_login" method="POST" action="index.php">
		Usuario <input type="text" name="user" id="user"><br>
		Contraseña <input type="password" name="pass" id="password"><br>
		<input type="submit" name="btnSubmit" value="Entrar">
	</form>

	<script type="text/javascript" src="assets/js/ajax.class.js"></script>
	<script type="text/javascript" src="assets/js/login.js"></script>
</body>
</html>
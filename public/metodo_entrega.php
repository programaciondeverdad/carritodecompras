<?php
include __DIR__.'/../autoload.php';
session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Carrito Compras</title>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
<body>
	<h1>Pagar</h1>
	Menu navegación:<br>
	<ul>
		<li><a href="login.php">Iniciar Sesión</a></li>
		<li><a href="registro.php">Registrarse</a></li>
		<li><a href="mi_carrito.php">Mi Carrito <?php echo "ID carrito: ".$_SESSION['idCarrito']; ?></a></li>
	</ul>

	<h2>Elegir método de entrega</h2>
	<form method="POST">
		<select name='metodo_entrega' id='metodo_entrega'>
		</select>
		<select name='metodo_entrega_rango' id='metodo_entrega_rango'>
		</select>
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Siguiente">
	</form>
	<h3>TODOs:</h3>
	<ol>
		<li>Pedirle la dirección al usuario para la entrega a domicilio.</li>
		<li>Pedirle la fecha de entrega</li>
	</ol>
	<br>

	<script type="text/javascript" src="assets/js/globals.js"></script>
	<script type="text/javascript" src="assets/js/ajax.class.js"></script>
	<script type="text/javascript" src="assets/js/metodo_entrega.js"></script>
	<script type="text/javascript">
		window.addEventListener("load", function() {
			var metodo_entrega = new MetodosEntrega();
			var metodo_entrega_listado = window.document.getElementById('metodo_entrega');
			metodo_entrega.verTodos(metodo_entrega_listado);
			metodo_entrega_listado.addEventListener('change', function(event)
			{
				metodo_entrega.cargarRangos(this.value);
			})
		});
		window.document.getElementById('btnSubmit').addEventListener("click", function()
		{
			event.preventDefault();
			var metodo_entrega = new MetodosEntrega();
			var metodo_entrega_id = window.document.getElementById('metodo_entrega').value;
			var metodo_entrega_rango = window.document.getElementById('metodo_entrega_rango').value;
			metodo_entrega.guardarMetodoEntrega(metodo_entrega_id, metodo_entrega_rango);
		});
	</script>
</body>
</html>
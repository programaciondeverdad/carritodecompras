class Ajax
{
	constructor()
	{
		
	}
	consultaAJAX(callbackOnDone)
	{
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				callbackOnDone(this);
			}
		};
		return xhttp;
	}
	

	myFunction(current, index)
	{
		return index + "=" + current;
	}

	get(url, params = [], callbackOnDone)
	{
		var xhttp = this.consultaAJAX(callbackOnDone);
		// Armado de parametros para pasar por URL
		// var query = params.map(this.myFunction2);
		var query = Object.keys(params).map(function(key) {
		  return [key + "=" + params[key]];
		});
		query = query.join('&');
		xhttp.open("GET", url + "?" + query, true);
		xhttp.send(); 
	}

	post(url, formData, callbackOnDone)
	{
		var xhttp = this.consultaAJAX(callbackOnDone);
		xhttp.open("POST", url, true);
		xhttp.send(formData);
	}
}
window.addEventListener("load", onLoad);
var valido = 0;
function onLoad()
{
	var listado_productos = window.document.getElementById('listado_productos');
	loadProductos(listado_productos);
}

function loadProductos(div)
{
	var self = this;
	
	var ajax = new Ajax();
	ajax.get("../routes/productos.php", [], function(ajaxResponse){
		var productos = JSON.parse(ajaxResponse.responseText)
		productos.map(function(item) {
			var producto_div = crearHTMLProducto(item);
			div.appendChild(producto_div);
		});
	});
	
}

function crearHTMLProducto(item)
{
	var producto_div = document.createElement("div");
	producto_div.classList.add("producto");
	producto_div.addEventListener("click", irAVerProducto);

	var titulo = agregarTexto("h3", item.nombre);
	producto_div.appendChild(titulo);

	if(item.descripcion != null)
	{
		var descripcion = agregarTexto("p", item.descripcion);
		producto_div.appendChild(descripcion);
	}

	var precio = agregarTexto("p", "$ "+item.precio);
	producto_div.appendChild(precio);

	var boton = agregarBoton("Ver producto");
	producto_div.appendChild(boton);
	boton.id = item.id;
	boton.addEventListener("click", irAVerProducto);

	return producto_div;
}
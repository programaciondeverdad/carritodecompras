class Producto
{
	constructor(){
	}
	
	loadDetalleProducto(div, id_producto)
	{
		var self = this;
		
		var ajax = new Ajax();
		
		ajax.get("../routes/productos.php", {idProducto: id_producto}, function(ajaxResponse){
			var producto = JSON.parse(ajaxResponse.responseText)

			self.crearHTMLProducto(producto);
		});
		
	}

	crearHTMLProducto(item)
	{
		var titulo = window.document.getElementById('titulo');
		titulo.innerHTML = item.nombre;

		if(item.descripcion != null)
		{
			var descripcion = window.document.getElementById('descripcion');
			descripcion.innerHTML = item.descripcion;
		}

		var precio = window.document.getElementById('precio');
		precio.innerHTML = "$ "+item.precio;
	}

}
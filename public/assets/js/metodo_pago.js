class MetodosPago
{
	constructor(){
	}
	verTodos(objecto_select)
	{
		var self = this;
		
		var ajax = new Ajax();

		ajax.get("../routes/metodos_entrega.php", [], function(ajaxResponse){
			var metodos_entrega = JSON.parse(ajaxResponse.responseText);
			metodos_entrega.unshift({id: null, nombre: '--Seleccione una opcion--'}); // Agrega al inicio
			metodos_entrega.map(function(item) {
				var metodo_entrega_opcion = self.crearHTMLMetodoEntrega(item);
				objecto_select.appendChild(metodo_entrega_opcion);
			});
		});

		
	}
	crearHTMLMetodoEntrega(item)
	{
		console.log(item);

		var option = agregarTexto("option", item.nombre);
		if(item.id != null)
		{
			option.value = item.id;
		}

		return option;
	}
	crearHTMLRango(item)
	{
		console.log(item);

		var option = null;
		if(item.id != null)
		{
			option = agregarTexto("option", item.descripcion + " (" + item.hora_inicio + " - " + item.hora_fin + ")");
			option.value = item.id;
		}
		else
		{
			option = agregarTexto("option", item.descripcion);
		}

		return option;
	}

	guardarMetodoPago(id)
	{
		var self = this;
		
		var ajax = new Ajax();
		var data = new FormData();
     	data.append('metodo_pago', id);

		ajax.post("../routes/metodos_pago.php", data, function(ajaxResponse){
			// Enviar al método de pago correspondiente
			// window.alert(ajaxResponse.responseText);
		});
	}
}
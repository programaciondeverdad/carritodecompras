window.addEventListener("load", onLoad);
var valido = 0;
function onLoad()
{
	var form_login = window.document.getElementById('form_login');
	form_login.addEventListener("submit", onSubmit);

	var user = window.document.getElementById('user');
	var password = window.document.getElementById('password');

	user.addEventListener("change", onChangeEmail);

	password.addEventListener("focusout", onChangePassword);
}

function onChangeEmail()
{
	if(!validarEmail(this.value))
	{
		alert("El Usuario tiene un formato incorrecto.");
		valido = -1;
	}
	else
	{
		valido = 1;
	}
	
}

function validarEmail(email) 
{
    var regex = /\S+@\S+\.\S+/;
		return regex.test(email);
}

function onChangePassword()
{
	if(this.value == "")
	{
		alert("La contraseña no puede estar vacía");
		valido = -1;
	}
	else
	{
		valido = 1;
	}
}

function onSubmit(event)
{
	var self = this;
	if(valido < 1)
	{
		alert("Revise los campos Usuario y Contraseña, y vuelva a intentarlo.");
		event.preventDefault();
		return false;
	}
	else
	{
		event.preventDefault();
		var data = new FormData();
     	data.append('user', document.getElementById("user").value);
        data.append('pass', document.getElementById("password").value);
			
		var ajax = new Ajax();
		ajax.post("../routes/login.php", data, function(ajaxResponse){
			window.location.href = self.action;			
		});
	}
}
class MetodosEntrega
{
	constructor(){
	}
	verTodos(objecto_select)
	{
		var self = this;
		
		var ajax = new Ajax();

		ajax.get("../routes/metodos_entrega.php", [], function(ajaxResponse){
			var metodos_entrega = JSON.parse(ajaxResponse.responseText);
			metodos_entrega.unshift({id: null, nombre: '--Seleccione una opcion--'}); // Agrega al inicio
			metodos_entrega.map(function(item) {
				var metodo_entrega_opcion = self.crearHTMLMetodoEntrega(item);
				objecto_select.appendChild(metodo_entrega_opcion);
			});
		});

		
	}
	cargarRangos(value)
	{
		var self = this;
		var listado_rangos = window.document.getElementById('metodo_entrega_rango');
		if(parseInt(value) > 0) // Solo cargo ragos si elegi un valor permitido
		{
			var ajax = new Ajax();
			ajax.get("../routes/metodos_entrega.php", {metodo_entrega_id: value}, function(ajaxResponse){
				listado_rangos.innerHTML = ""; // limpiamos
				var rangos = JSON.parse(ajaxResponse.responseText);
				rangos.unshift({id: null, descripcion: '--Seleccione una opcion--'}); // Agrega al inicio
				rangos.map(function(item) {
					var rango_opcion = self.crearHTMLRango(item);
					listado_rangos.appendChild(rango_opcion);
				});
			});
		}
		
	}
	crearHTMLMetodoEntrega(item)
	{
		console.log(item);

		var option = agregarTexto("option", item.nombre);
		if(item.id != null)
		{
			option.value = item.id;
		}

		return option;
	}
	crearHTMLRango(item)
	{
		console.log(item);

		var option = null;
		if(item.id != null)
		{
			option = agregarTexto("option", item.descripcion + " (" + item.hora_inicio + " - " + item.hora_fin + ")");
			option.value = item.id;
		}
		else
		{
			option = agregarTexto("option", item.descripcion);
		}

		return option;
	}

	guardarMetodoEntrega(metodo_entrega_id, metodo_entrega_rango)
	{
		var self = this;
		
		var ajax = new Ajax();
		var data = new FormData();
     	data.append('metodo_entrega', metodo_entrega_id);
        data.append('rango', metodo_entrega_rango);

		ajax.post("../routes/metodos_entrega.php", data, function(ajaxResponse){
			window.location.href = 'metodo_pago.php';
		});
	}
}
function agregarTexto(htmlElementTipo, texto)
{
	var elementoHTML = document.createElement(htmlElementTipo);
	var node_texto = document.createTextNode(texto);
	elementoHTML.appendChild(node_texto);

	return elementoHTML;
}

function agregarBoton(texto)
{
	return agregarTexto("button", texto);
}

function irAVerProducto()
{
	window.location.href = "producto.php?id="+this.id;
}
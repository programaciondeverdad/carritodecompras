window.addEventListener("load", onLoad);
var valido = 0;
function onLoad()
{
	var form_registro = window.document.getElementById('form_registro');
	form_registro.addEventListener("submit", onSubmit);
	var email = window.document.getElementById('email');
	var password = window.document.getElementById('password');
	var repassword = window.document.getElementById('repassword');

	email.addEventListener("change", onChangeEmail);

	password.addEventListener("focusout", onChangePasswords);

	repassword.addEventListener("focusout", onChangePasswords);
}

function onChangeEmail()
{
	if(!validarEmail(this.value))
	{
		alert("El E-mail tiene un formato incorrecto.");
		valido = -1;
	}
	else
	{
		valido = 1;
	}
	
}

function validarEmail(email) 
{
    var regex = /\S+@\S+\.\S+/;
		return regex.test(email);
}

function onChangePasswords()
{
	var password = window.document.getElementById('password');
	var repassword = window.document.getElementById('repassword');
	if(password.value != repassword.value && 
		password.value != "" && 
		repassword.value != "")
	{
		alert("Las contraseñas no coinciden");
		valido = -1;
	}
	else
	{
		valido = 1;
	}
}

function onSubmit(event)
{
	if(valido < 1)
	{
		alert("Revise los campos E-mail y Passwords y vuelva a intentarlo.");
		event.preventDefault();
		return false;
	}
	else
	{
		event.preventDefault();
		var data = new FormData();
     	data.append('user', document.getElementById("email").value);
        data.append('pass', document.getElementById("password").value);
			
		var ajax = new Ajax();
		ajax.post("../routes/register.php", data, function(ajaxResponse){
			alert(ajaxResponse.responseText);
			window.location.href = "login.php";
		});
	}
}
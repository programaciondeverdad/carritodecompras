class Carrito
{
	constructor(){
	}
	verCarrito(div)
	{
		var self = this;
		
		var ajax = new Ajax();

		ajax.get("../routes/carrito.php", [], function(ajaxResponse){
			var productos = JSON.parse(ajaxResponse.responseText)
			productos.map(function(item) {
				var producto_div = self.crearHTMLProducto(item);
				div.appendChild(producto_div);
			});
		});
		
	}
	verInputHiddens(div)
	{
		var self = this;
		
		var ajax = new Ajax();

		ajax.get("../routes/carrito.php", [], function(ajaxResponse){
			var productos = JSON.parse(ajaxResponse.responseText)
			productos.map(function(item) {
				var producto = self.crearInputHidden(item);
				div.appendChild(producto);
			});
		});
		
	}
	agregarProducto(id_producto, cantidad)
	{
		var self = this;
		
		var ajax = new Ajax();
		var data = new FormData();
     	data.append('idProducto', id_producto);
        data.append('cantidad', cantidad);

		ajax.post("../routes/carrito.php", data, function(ajaxResponse){
			window.alert(ajaxResponse.responseText);
		});
		
	}
	crearHTMLProducto(item)
	{
		// console.log(item);
		var producto_div = document.createElement("div");
		producto_div.classList.add("producto");

		var titulo = agregarTexto("h3", item.nombre);
		producto_div.appendChild(titulo);

		var precio = agregarTexto("p", "$ "+item.precio);
		producto_div.appendChild(precio);

		var boton = agregarBoton("Ver producto");
		producto_div.appendChild(boton);
		boton.id = item.id;
		boton.addEventListener("click", irAVerProducto);
		return producto_div;
	}
	crearInputHidden(item)
	{
		var producto_input = document.createElement("input");
		producto_input.setAttribute("type", "hidden");
		producto_input.setAttribute("name", "items[]");
		producto_input.value = item.precio * item.cantidad;
		return producto_input;
	}
}
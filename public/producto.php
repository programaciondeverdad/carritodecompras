<?php
include __DIR__.'/../autoload.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Carrito Compras</title>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
<body>
	<h1>Carrito de compras - Inicio</h1>
	Menu navegación:<br>
	<ul>
		<li><a href="login.php">Iniciar Sesión</a></li>
		<li><a href="registro.php">Registrarse</a></li>
		<li><a href="mi_carrito.php">Mi Carrito</a></li>
	</ul>


	<div id='detalle_producto'>
		<h1 id='titulo'>Productos</h1>
		<p id="descripcion"></p>
		<p id="precio"></p>
		<form method="POST">
			<input type="hidden" name="idProducto" id="idProducto" value="<?php echo $_GET['id']; ?>">
			<input type="number" name="cantidad" id="cantidad" value='1'><br>
			<input type="submit" name="btnAgregar" id="btnAgregar" value="Agregar Carrito">
		</form>
	</div>

	<script type="text/javascript" src="assets/js/ajax.class.js"></script>
	<script type="text/javascript" src="assets/js/producto.js"></script>
	<script type="text/javascript" src="assets/js/carrito.js"></script>
	<script type="text/javascript">
		window.addEventListener("load", function() {
			var producto = new Producto();
			var detalle_producto = window.document.getElementById('detalle_producto');
			var id_producto = window.document.getElementById('idProducto').value;
			producto.loadDetalleProducto(detalle_producto, id_producto);
		});

		window.document.getElementById('btnAgregar').addEventListener("click", function()
		{
			event.preventDefault();
			var carrito = new Carrito();
			var id_producto = window.document.getElementById('idProducto').value;
			var cantidad = window.document.getElementById('cantidad').value;
			carrito.agregarProducto(id_producto, cantidad);
		});
	</script>
</body>
</html>
<?php
include __DIR__.'/../autoload.php';
session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Carrito Compras</title>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
</head>
<body>
	<h1>Pagar</h1>
	Menu navegación:<br>
	<ul>
		<li><a href="login.php">Iniciar Sesión</a></li>
		<li><a href="registro.php">Registrarse</a></li>
		<li><a href="mi_carrito.php">Mi Carrito <?php echo "ID carrito: ".$_SESSION['idCarrito']; ?></a></li>
	</ul>

	<h2>Elegir método de pago</h2>

	<a href="terminar_carrito.php">Terminar</a>
	<br>

	<script type="text/javascript" src="assets/js/globals.js"></script>
	<script type="text/javascript" src="assets/js/ajax.class.js"></script>
	<script type="text/javascript" src="assets/js/producto.js"></script>
	<script type="text/javascript" src="assets/js/carrito.js"></script>
	<script type="text/javascript">
		window.addEventListener("load", function() {
			var carrito = new Carrito();
			var listado_productos = window.document.getElementById('listado_productos');
			carrito.verCarrito(listado_productos);
		});
	</script>
</body>
</html>
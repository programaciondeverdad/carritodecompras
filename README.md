# README #

Curso de Carrito de Compras en PHP + JS + MYSQL (https://www.youtube.com/playlist?list=PLJTVBLjDuH-TOcVpM_h2A9RnceiwLCs2q) de Programación de Verdad (https://www.instagram.com/programacion_de_verdad/)

### ¿Qué tiene este repositorio? ###

Este repositorio contiene el código que usamos en los videos del curos Carrito de Compras (https://www.youtube.com/playlist?list=PLJTVBLjDuH-TOcVpM_h2A9RnceiwLCs2q).

### ¿Cómo está organizado? ###

* master: Última versión del código. Recopila el código de todos los branchs.
* parteX_nombre: El resto de los branchs tienen el código que se utiliza en cada video.

### ¿Cómo utilizar el código? ###

* Ir a https://bitbucket.org/programaciondeverdad/carritodecompras/downloads/?tab=branches
* Buscar el branch que deseas descargar según el video que vas a ver
* Descomprimir el .zip dentro de una carpeta de WAMP/XAMP o similar. Por ejemplo C:\wamp\www\carrito_compras
* Iniciar el servidor localhost (WAMP/XAMP o similares)
* Ir a http://localhost/carrito_compras

### ¿Querés colaborar? ###

Podés escribirnos a programaciondeverdad@gmail.com o contactarnos por las redes sociales buscándonos como Programación De Verdad.

### ¿Necesitás ayuda? ###

Podés escribirnos a programaciondeverdad@gmail.com o contactarnos por las redes sociales buscándonos como Programación De Verdad.
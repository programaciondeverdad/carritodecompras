<?php
class UsuariosRepository extends Repository
{
	public function __construct(CarritosRepository $carritosRepository, 
								ProductosRepository $productosRepository)
	{
		$this->carritosRepository = $carritosRepository;
		$this->productosRepository = $productosRepository;
		parent::__construct();
	}

	public function getTable()
	{
		return 'usuarios';
	}
	
}
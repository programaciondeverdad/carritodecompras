<?php
class Repository
{
	/* mysqli conexión */
	private $conn;
	private $sql;
	private $result;
	private $objectsArray = [];

	public function __construct()
	{
		// Nos tenemos que conectar a la base de datos MySQL! 
		// Obtener los datos de la tabla usando SELECT con WHERE
		$servername = "localhost";
		$username = "root";
		$password = "";
		$schema = "carrito_compras";

		// Create connection
		$this->conn = new mysqli($servername, $username, $password, $schema);

		// Check connection
		if ($this->getConn()->connect_error) {
		    throw new Exception("Connection failed: " . $this->getConn()->connect_error);
		}

	}

	public function findAll()
	{
		$table = $this->getTable();
		// Consulta SQL, pedimos que lo encuentre con el id
		$this->setSql("SELECT * FROM $table
		WHERE estado = 'A'");
		
		// Ejecutamos la consulta
		if ($this->setResult($this->getConn()->query($this->getSql()))) {  // if ($result = $conn -> query($sql)) {
			// Mientras haya resultados... Devolver una variable $obj con los datos de la fila actual
			while ($obj = $this->getResult()->fetch_object()) {
				$this->addObjectsArray($obj);
		    }
		    // Liberamos las resultados para que pueda ser usado por otra persona o sesión (la tabla)
		  	$this->getResult()->free_result();
			$this->getConn()->close();
		  	return $this->getObjectsArray();
		}
		else
		{
			http_response_code(500);
			throw new Exception("Error al ejecutar la consulta.", 1);
			
		}
	}


	public function findOneBy($keysValues)
	{
		$table = $this->getTable();
		
		$sql = "SELECT * FROM $table
		WHERE 1 ";
		$this->setSql($sql)->addWhere($keysValues);
		$sql = $this->getSql() . " LIMIT 1 ";

		// Ejecutamos la consulta
		if ($this->setResult($this->getConn()->query($this->getSql()))) {  // if ($result = $conn -> query($sql)) {
			// Mientras haya resultados... Devolver una variable $obj con los datos de la fila actual
			if($this->getResult())
			{
				if ($obj = $this->getResult()->fetch_object()) {
					$this->addObjectsArray($obj);

					// Liberamos las resultados para que pueda ser usado por otra persona o sesión (la tabla)
				  	$this->getResult()->free_result();

					$response = $this->getObjectsArray()[0];
					return $response->id;
			    }
			    else
			    {
			    	return null;
			    }
			}
			else
			{
				return null;
			}

		}
		else
		{
			http_response_code(500);
			throw new Exception("Error al ejecutar la consulta.", 1);
			
		}
	}

	public function findAllBy($keysValues)
	{
		$table = $this->getTable();
		
		$sql = "SELECT * FROM $table
		WHERE 1 ";
		$this->setSql($sql)->addWhere($keysValues);
		
		return $this->executeAll();
	}

	public function findBy($keysValues)
	{
		$table = $this->getTable();
		
		$sql = "SELECT * FROM $table
		WHERE 1 ";
		$this->setSql($sql)->addWhere($keysValues);
		$sql = $this->getSql() . " LIMIT 1 ";
		
		// Ejecutamos la consulta
		return $this->executeAll();
	}

	public function create($object)
	{

	}

	protected function executeAll()
	{
		// Ejecutamos la consulta
		if ($this->setResult($this->getConn()->query($this->getSql()))) {  // if ($result = $conn -> query($sql)) {
			// Mientras haya resultados... Devolver una variable $obj con los datos de la fila actual
			if($this->getResult())
			{
				while ($obj = $this->getResult()->fetch_object()) {
					$this->addObjectsArray($obj);
			    }
				// Liberamos las resultados para que pueda ser usado por otra persona o sesión (la tabla)
			  	$this->getResult()->free_result();

				$response = $this->getObjectsArray();
				return $response;
			}
			else
			{
				return null;
			}

		}
		else
		{
			http_response_code(500);
			throw new Exception("Error al ejecutar la consulta.", 1);
			
		}
	}

	public function addWhere($keysValues = [])
	{
		if(count($keysValues) > 0)
		{
			$this->sql .= " AND ";
		}

		foreach ($keysValues as $key => $value) {
			$toFind[] = "{$key} = '{$value}'";
		}
		$where = implode(" AND ", $toFind);
		$this->sql .= $where;
		return $this;
	}

	public function getConn()
	{
		return $this->conn;
	}

	public function getSql()
	{
		return $this->sql;
	}
	public function setSql($sql)
	{
		$this->sql = $sql;
		return $this;
	}

	public function getResult()
	{
		return $this->result;
	}
	public function setResult($result)
	{
		$this->result = $result;
		return $this;
	}

	public function getObjectsArray()
	{
		return $this->objectsArray;
	}
	public function setObjectsArray($objectsArray)
	{
		$this->objectsArray = $objectsArray;
		return $this;
	}
	public function addObjectsArray($object)
	{
		array_push($this->objectsArray, $object);
		return $this;
	}
}
<?php
class MetodosPagoRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getTable()
	{
		return 'metodos_pago';
	}

	public function getRangosMetodoPago($where)
	{
		$table = $this->getTable();
		$sql = "SELECT 
					metodos_pago_rangos.id,
					metodos_pago_rangos.hora_inicio,
					metodos_pago_rangos.hora_fin,
					metodos_pago_rangos.descripcion
					 FROM {$table} 
			INNER JOIN metodos_pago_rangos ON {$table}.id = metodos_pago_rangos.metodo_pago_id
		";
		$this->setSql($sql)->addWhere($where);
		return $this->executeAll();
	}

	public function create($data)
	{
		$table = $this->getTable();
		session_start();
		// var_dump($usuario_id);
		$carrito_usuario = $_SESSION['idCarrito'];
		
		$metodo_pago_id = $data['metodo_pago_id'];
		$metodo_pago_rango_id = $data['metodo_pago_rango_id'];

		$sql = "UPDATE carritos SET metodo_pago_id = {$metodo_pago_id}, metodo_pago_rango_id = {$metodo_pago_rango_id}
		WHERE 1";
		$this->setSql($sql)->addWhere(['id' => $carrito_usuario]);

		// Ejecutamos la consulta
		if ($this->getConn()->query($this->getSql()) === TRUE) {
		  echo "OK!";
		} else {
			// En vez de devolver un mensaje, devolvemos un error. PHP devolverá error HTTP 500 en vez de HTTP 200
			http_response_code(500);
			throw new Exception("Error: " . $sql . "<br>" . $this->getConn()->error, 1);
		}
		
	}


	
}
<?php
class CarritosProductosRepository extends Repository
{
	public function __construct(CarritosRepository $carritosRepository, 
								ProductosRepository $productosRepository,
								UsuariosRepository $usuariosRepository)
	{
		$this->carritosRepository = $carritosRepository;
		$this->productosRepository = $productosRepository;
		$this->usuariosRepository = $usuariosRepository;
		parent::__construct();
	}

	public function getTable()
	{
		return 'carritos_productos';
	}

	public function create($objectArr)
	{
		$table = $this->getTable();
		
		// Primero buscar que existan carrito y producto
		// Llamamos a los repositorios

		session_start(); // Iniciamos session de php
		if(isset($_SESSION['idCarrito']) && !is_null($_SESSION['idCarrito']))
		{
			$carrito = $this->carritosRepository->findOneBy(['id'=>$_SESSION['idCarrito']]);
		}
		else{
			$usuario = $this->usuariosRepository->findOneBy(['user' => $_SESSION['user']]); // buscamos el usuario
			$carrito = $this->carritosRepository->create(['usuario_id'=>$usuario->id]); // creamos un carrito para este usuario
		}

		$producto = $this->productosRepository->findOneBy(['id'=>$objectArr['idProducto']]);

		return $this->insertOrUpdate($producto, $carrito, $objectArr['cantidad']);
	}



	public function insertOrUpdate($producto_id, $carrito_id, $cantidad)
	{
		$table = $this->getTable();
		// var_dump($usuario_id);
		$carrito_producto = $this->findProductoByCarrito($producto_id, $carrito_id);
		
		// Si no tiene el producto agregado al carrito de compras...
		if(is_null($carrito_producto)){
			// Lo agrega
			return $this->insert($producto_id, $carrito_id, $cantidad);
		}
		else
		{
			return $this->updateCantidad($carrito_id, $producto_id, $cantidad);// Si ya existe... Actualizamos
		}
	}

	public function insert($producto_id, $carrito_id, $cantidad)
	{
		$table = $this->getTable();
		$sql = "INSERT INTO {$table} (carrito_id, producto_id, cantidad, estado)
		VALUES('{$carrito_id}', '{$producto_id}', '{$cantidad}', 'P')";
		$this->setSql($sql);
		
		// Ejecutamos la consulta
		if ($this->getConn()->query($this->getSql()) === TRUE) { // Ejecutamos el sql y comprobamos
		  // echo "Carrito Creado con exito creado con éxito!";
		  return $this->findProductoByCarrito($producto_id, $carrito_id); // Devolvemos...
		} else {
			// En vez de devolver un mensaje, devolvemos un error. PHP devolverá error HTTP 500 en vez de HTTP 200
			http_response_code(500);
			throw new Exception("Error: " . $sql . "<br>" . $this->getConn()->error, 1);
		}
	}

	public function updateCantidad($carrito_id, $producto_id, $cantidad)
	{
		$table = $this->getTable();
		$sql = "UPDATE {$table} SET cantidad = cantidad+{$cantidad} 
			WHERE 1 ";
			$this->setSql($sql)->addWhere([
				'carrito_id' => $carrito_id,
				'producto_id' => $producto_id
			]);
			
			// Ejecutamos la consulta
			if ($this->getConn()->query($this->getSql()) === TRUE) { // Actualizamos y comprobamos 
			  return $this->findProductoByCarrito($producto_id, $carrito_id); // Devolvemos el resultado
			} else {
				// En vez de devolver un mensaje, devolvemos un error. PHP devolverá error HTTP 500 en vez de HTTP 200
				http_response_code(500);
				throw new Exception("Error: " . $sql . "<br>" . $this->getConn()->error, 1);
			}
	}

	public function findProductoByCarrito($producto_id, $carrito_id)
	{
		return $this->findOneBy([
			'producto_id' => $producto_id,
			'carrito_id' => $carrito_id
		]);		
	}

	public function getProductosByCarrito($data)
	{
		$table = $this->getTable();
		$sql = "SELECT * FROM {$table} 
			INNER JOIN productos ON {$table}.producto_id = productos.id
		";
		$this->setSql($sql)->addWhere([
				'carrito_id' => $data['carrito_id']
			]);
		
		// Ejecutamos la consulta
		if ($this->setResult($this->getConn()->query($this->getSql()))) {  // if ($result = $conn -> query($sql)) {
			// Mientras haya resultados... Devolver una variable $obj con los datos de la fila actual
			if($this->getResult())
			{
				while ($obj = $this->getResult()->fetch_object()) {
					$this->addObjectsArray($obj);
			    }
				// Liberamos las resultados para que pueda ser usado por otra persona o sesión (la tabla)
			  	$this->getResult()->free_result();

				$response = $this->getObjectsArray();
				return $response;
			}
			else
			{
				return null;
			}

		}
		else
		{
			http_response_code(500);
			throw new Exception("Error al ejecutar la consulta.", 1);
			
		}
	}


	
}
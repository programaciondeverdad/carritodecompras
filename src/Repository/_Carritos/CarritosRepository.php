<?php
class CarritosRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getTable()
	{
		return 'carritos';
	}

	public function create($usuario_id)
	{
		$table = $this->getTable();
		// var_dump($usuario_id);
		$carrito_usuario = $this->findLastActiveCarritoByIdUsuario($usuario_id);
		// Si no tiene carrito de compras, lo crea
		if(is_null($carrito_usuario)){
			$fecha_hoy = new \DateTime();
			$fecha_registro = $fecha_hoy->format('Y-m-d');

			$sql = "INSERT INTO {$table} (usuario_id, fecha_registro, estado)
			VALUES('{$usuario_id}', '{$fecha_registro}', 'A')";
			$this->setSql($sql);

			// var_dump($sql);
			
			// Ejecutamos la consulta
			if ($this->getConn()->query($this->getSql()) === TRUE) {
			  // echo "Carrito Creado con exito creado con éxito!";
			  return $this->findLastActiveCarritoByIdUsuario($usuario_id);
			} else {
				// En vez de devolver un mensaje, devolvemos un error. PHP devolverá error HTTP 500 en vez de HTTP 200
				http_response_code(500);
				throw new Exception("Error: " . $sql . "<br>" . $this->getConn()->error, 1);
			}
		}
		else
		{
			return $carrito_usuario;
		}
	}

	public function findLastActiveCarritoByIdUsuario($usuario_id)
	{
		$table = $this->getTable();
		$sql = "SELECT * FROM $table
		WHERE 1 ";
		$this->setSql($sql)->addWhere([
			'usuario_id' => $usuario_id,
			'estado' => 'A'
		]);

		$this->setSql($this->getSql() . " ORDER BY fecha_registro DESC LIMIT 1 ");

		// Ejecutamos la consulta
		if ($this->setResult($this->getConn()->query($this->getSql()))) {  // if ($result = $conn -> query($sql)) {
			// Mientras haya resultados... Devolver una variable $obj con los datos de la fila actual
			if($this->getResult())
			{
				if ($obj = $this->getResult()->fetch_object()) {
					$this->addObjectsArray($obj);

					// Liberamos las resultados para que pueda ser usado por otra persona o sesión (la tabla)
				  	$this->getResult()->free_result();
					$this->getConn()->close();

					$response = $this->getObjectsArray()[0];
					var_dump($response);
					return $response->id;
			    }
			    else
			    {
			    	return null;
			    }
			}
			else
			{
				return null;
			}

		}
		else
		{
			http_response_code(500);
			throw new Exception("Error al ejecutar la consulta.", 1);
			
		}
	}
	
}
<?php
class ProductosRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getTable()
	{
		return 'productos';
	}
	
}
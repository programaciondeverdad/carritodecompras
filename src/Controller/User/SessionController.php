<?php
class SessionController extends Controller
{
	public function __construct(CarritosRepository $carritosRepository)
	{
		$this->carritosRepository = $carritosRepository;
		parent::__construct();
	}
	public function logIn($request)
	{
		session_start();
		if(isset($request['user']) && $request['pass'])
		{
			$user = $request['user'];
			$pass = $request['pass'];
			if(!empty($user) && !empty($pass))
			{
				// Si no viste el video de Bases de datos: 

				// Nos tenemos que conectar a la base de datos MySQL! 
				// Obtener los datos de la tabla usando SELECT con WHERE
				$servername = "localhost";
				$username = "root";
				$password = "";
				$schema = "carrito_compras";

				// Create connection
				$conn = new mysqli($servername, $username, $password, $schema);

				// Check connection
				if ($conn->connect_error) {
				    throw new Exception("Connection failed: " . $conn->connect_error);
				}

				// Consulta SQL, pedimos que lo encuentre con el id
				$sql = "SELECT id, user, password, fecha_registro, estado FROM usuarios 
				WHERE user = '$user' AND estado = 'A'";
				
				// Ejecutamos la consulta
				if ($result = $conn -> query($sql)) {
					// Mientras haya resultados... Devolver una variable $obj con los datos de la fila actual
					if ($obj = $result->fetch_object()) {
						// Al haber usado password_hash($password, PASSWORD_DEFAULT), la forma que nos ofrece PHP para validar es usando password_verify.
						if(password_verify($pass, $obj->password))
						{
							$_SESSION['user'] = $obj->user;
							$_SESSION['fecha_registro'] = $obj->fecha_registro;
							$_SESSION['rol'] = 'USER_NORMAL'; // TODO: asignar una clase específica para roles.
							$_SESSION['isLogged'] = true;
							$_SESSION['idCarrito'] = $this->carritosRepository->create($obj->id); // Buscar en el repository el último carrito de compras que esté en estado ACTIVO.
						}
						else
						{
							http_response_code(403);
							throw new Exception("La contraseña es incorrecta.", 1);
							
						}
				    }
				    // Liberamos las resultados para que pueda ser usado por otra persona o sesión (la tabla)
				  	$result->free_result();
				}
				else
				{
					http_response_code(500);
					throw new Exception("Error al ejecutar la consulta.", 1);
					
				}

				$conn->close();
			}
		}

		if(isset($_SESSION['isLogged']))
		{
			if($_SESSION['isLogged'] === true)
			{
				echo "Iniciado sesión! Hola ".$_SESSION['user'] . "<br>";
				// echo "<a href='logout.php'>Log Out</a>";
			}
			else
			{
				http_response_code(403);
				throw new Exception("Usted no tiene permisos para ver esta página.", 1);
				  

			}
		}else
		{
			http_response_code(403);
			throw new Exception("No se ha logueado correctamente. Intentelo nuevamente. <a href='login.php'>Volver</a>", 1);
			  
		}
	}

	public function logOut()
	{
		// Inicializar la sesión.
		// Si está usando session_name("algo"), ¡no lo olvide ahora!
		session_start();

		// Destruir todas las variables de sesión.
		$_SESSION = array();

		// Si se desea destruir la sesión completamente, borre también la cookie de sesión.
		// Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		// Finalmente, destruir la sesión.
		session_destroy();

		echo "Sesión cerrada. <a href='login.php'>Volver</a>";
	}
}


?>
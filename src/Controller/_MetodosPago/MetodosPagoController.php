<?php
class MetodosPagoController extends Controller
{
	private $metodosPagoRepository;

	public function __construct(MetodosPagoRepository $metodosPagoRepository)
	{
		$this->metodosPagoRepository = $metodosPagoRepository;
	}
	public function index()
	{
		$this->json_response($this->metodosPagoRepository->findAll());
	}

	public function get($request)
	{
		$where = [
			'metodo_pago_id' => $request['metodo_pago_id'],
			'estado' => 'A'
		];
		$response = $this->metodosPagoRepository->getRangosMetodoPago($where);
		$this->json_response($response);
	}

	
	public function post($request)
	{
		// Intenta crear o actualizar
		$result = $this->metodosPagoRepository->create([
															'metodo_pago_id' 			=> $request['metodo_pago'], 
														]);

		// Si responde algo...

		if($result)
		{
			// Significa que lo pudo agregar OK.
			echo 'Método de pago guardado con éxito.';
		}

	}
	
}
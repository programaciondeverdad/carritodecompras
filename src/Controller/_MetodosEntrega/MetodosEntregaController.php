<?php
class MetodosEntregaController extends Controller
{
	private $metodosEntregaRepository;

	public function __construct(MetodosEntregaRepository $metodosEntregaRepository)
	{
		$this->metodosEntregaRepository = $metodosEntregaRepository;
	}
	public function index()
	{
		$this->json_response($this->metodosEntregaRepository->findAll());
	}

	public function get($request)
	{
		$where = [
			'metodo_entrega_id' => $request['metodo_entrega_id'],
			'metodos_entrega_rangos.estado' => 'A'
		];
		$response = $this->metodosEntregaRepository->getRangosMetodoEntrega($where);
		$this->json_response($response);
	}

	
	public function post($request)
	{
		// Intenta crear o actualizar
		$result = $this->metodosEntregaRepository->create([
															'metodo_entrega_id' 			=> $request['metodo_entrega'], 
															'metodo_entrega_rango_id' 		=> $request['rango']
														]);

		// Si responde algo...

		if($result)
		{
			// Significa que lo pudo agregar OK.
			echo 'Método de entrega guardado con éxito.';
		}

	}
	
}
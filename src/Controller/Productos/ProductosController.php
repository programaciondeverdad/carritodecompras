<?php
class ProductosController extends Controller
{
	public function __construct(ProductosRepository $productosRepository)
	{
		$this->productosRepository = $productosRepository;
	}
	public function index()
	{
		$this->json_response($this->productosRepository->findAll());
	}
	public function get($request)
	{
		$result = $this->productosRepository->findOneBy(['id'=>$_GET['idProducto']]);
		$this->json_response($result);
	}
	
}
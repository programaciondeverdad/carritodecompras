<?php
class CarritosController extends Controller
{
	private $carritosProductosRepository;
	private $carritosRepository;

	public function __construct(CarritosProductosRepository $carritosProductosRepository,
								CarritosRepository $carritosRepository)
	{
		$this->carritosProductosRepository = $carritosProductosRepository;
		$this->carritosRepository = $carritosRepository;
	}
	public function index()
	{
		session_start();
		$this->json_response($this->carritosProductosRepository->getProductosByCarrito(['carrito_id' => $_SESSION['idCarrito']]));
	}
	public function get($request)
	{
		$result = $this->carritosRepository->findOneBy(['id'=>$request['idCarrito']])[0];
		$this->json_response($result);
	}
	/* Agregar producto al carrito de compras */
	public function post($request)
	{
		// Intenta crear o actualizar
		$result = $this->carritosProductosRepository->create([
															'idProducto' 	=> $request['idProducto'], 
															'cantidad' 		=> $request['cantidad']
														]);

		// Si responde algo...

		if($result)
		{
			// Significa que lo pudo agregar OK.
			echo 'Producto Agregado con Éxito';
		}

	}
	
}
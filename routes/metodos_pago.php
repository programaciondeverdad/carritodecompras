<?php
include __DIR__.'/../autoload.php';

$metodosPagoRepository = new MetodosPagoRepository();

$metodosPagoController = new MetodosPagoController($metodosPagoRepository); // Design Pattern: Injection
if(isset($_GET['metodo_pago_id']))
{
	// devolver todos los rangos de este metodo de pago
	$metodosPagoController->get($_GET);
}
elseif(isset($_POST['metodo_pago']) && isset($_POST['rango']))
{
	$metodosPagoController->post($_POST);
}
else
{
	// devolver todos los metodos de pago
	$metodosPagoController->index();
}
?>
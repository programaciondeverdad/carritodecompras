<?php
include __DIR__.'/../autoload.php';

$carritosRepository = new CarritosRepository();
$productosRepository = new ProductosRepository();

$usuariosRepository = new UsuariosRepository($carritosRepository, $productosRepository);

$carritosProductosRepository = new CarritosProductosRepository($carritosRepository, $productosRepository, $usuariosRepository);

$carritosController = new CarritosController($carritosProductosRepository, $carritosRepository); // Design Pattern: Injection
if(isset($_POST['idProducto']) && isset($_POST['cantidad']))
{
	$carritosController->post($_POST);
}
else
{
	$carritosController->index();
}
?>
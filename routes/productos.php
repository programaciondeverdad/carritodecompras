<?php
include __DIR__.'/../autoload.php';

$productosRepository = new ProductosRepository();
$productosController = new ProductosController($productosRepository); // Design Pattern: Injection
if(isset($_GET['idProducto']))
{
	$productosController->get($_GET);
}
else
{
	$productosController->index();
}
?>
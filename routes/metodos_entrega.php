<?php
include __DIR__.'/../autoload.php';

$metodosEntregaRepository = new MetodosEntregaRepository();

$metodosEntregaController = new MetodosEntregaController($metodosEntregaRepository); // Design Pattern: Injection
if(isset($_GET['metodo_entrega_id']))
{
	// devolver todos los rangos de este metodo de entrega
	$metodosEntregaController->get($_GET);
}
elseif(isset($_POST['metodo_entrega']) && isset($_POST['rango']))
{
	$metodosEntregaController->post($_POST);
}
else
{
	// devolver todos los metodos de entrega
	$metodosEntregaController->index();
}
?>